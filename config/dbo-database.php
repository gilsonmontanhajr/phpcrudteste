<?php

    /*
    |--------------------------------------------------------------------------
    | Classe Database
    |--------------------------------------------------------------------------
    |
    | Classe utilizada para interações com banco de dados
    */

//require_once __DIR__.'/config.php';
require_once "config.php";
	
class DBODatabase {
 
	private $conn;
    
	private function startDb($selector){
		
	        $config = include __DIR__.'/config.php';
		
		if ($selector ==  '')
			$selector = $config['default'];
		
		$host = $config['connections'][$selector]['host'];
		$dbname = $config['connections'][$selector]['database'];
		$charset = $config['connections'][$selector]['charset'];

		$this->conn = new PDO("mysql:host=$host;dbname=$dbname;charset=$charset", $config['connections'][$selector]['username'], $config['connections'][$selector]['password']);
		$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	}
	
	public function execNonQuery( $sql ){
		$this->conn->query( $sql );
	}
	
	public function execQuery( $sql ){
		return $this->conn->query( $sql );
	}

	
        public function prepare( $sql ){
                return $this->conn->prepare( $sql );
        }


	public function getError(){
		return $this->conn->errorInfo();
	}
	
	function __destruct() {
		try{
			//mysql_close($this->conn);
		} catch (Exception $ex) {}
	}
	
	function __construct($selector) {
       		self::startDb($selector);
	}
		
}   


