<?php

/*
 Iniciando nosso script PHP para fazer o cadastro.

 O que este arquivo deve fazer ?
 * Receber os dados que foram digitados no cadastro.php
 * Tratar estes dados de maneira que não deixe ser cadastrado se os campos estiverem em branco.
 * Caso esteja tudo ok, fazer o cadastro no banco de dados.

*/

// Iniciaremos incluindo a classe responsável pela conexão com o banco de dados.
require_once "config/dbo-database.php";

/*
 Criando o objeto de conexão com o banco de dados;
 * O nome $conexaoComBancoDeDados é a variável que vai receber a instancia do objeto.
 * O parâmetro new, indica que será instanciado um novo objeto.
 * O DBODatabase é a classe responsável por criar o objeto.
 * O parametro crud é o nome da conexão com o banco de dados, que foi colocado no arquivo config.php, dentro do diretório config.
*/
$conexaoComBancoDeDados = new DBODatabase('crud');

/*
 Agora receberemos os campos que foram digitados no formulário
 O parametro S_POST, é responsável por receber os campos que vem via método POST do formulário (não aparece na barra).
 em outra tela, faremos o teste com o GET (que passa parametros pelo endereço).
*/
$nome = $_POST['nome'];
$endereco = $_POST['endereco'];

echo "<h1>Resultado da solicitação de cadastro !</h1>";

// Agora trataremos as variáveis
if (isset($nome) and isset($endereco)){
    // Tenta executar alguma coisa.
    // Sempre é usado em caso de execução de banco de dados.
    try{
        // Aqui temos o ponto, que serve de concatenação, o resto do comando é uma SQL simples de inserção.
        $cadastraNovo = "INSERT INTO crudizinho (nome, endereco) VALUES (\"".$nome."\",\"".$endereco."\")";
        // Utilizando o objeto de conexão que foi criado acima, para executar um comando SQL.
        echo $nome . ", " . $endereco;
        $conexaoComBancoDeDados->execQuery($cadastraNovo);
    } catch (PDOException $e){
        // Em caso de retorno de excessão (erro) ele vai mostrar qual é o código do erro.
        echo "Erro !" . $e->getMessage();
    }
} else {
    // Se o campo nome estiver vazio.
    echo "O campo nome é requerido ! <br/>";
    // Se o campo endereço estiver vazio
    echo "O Campo endereço é requerido ! <br/>";
}
?>
<br/><br/>
<a href="index.php">Voltar para Index</a>
