<?php
/*
 * Este arquivo vai ser um tanto diferente do cadastro.
 * Neste arquivo teremos código PHP misturado com HTML.
 *
 * Inicialmente, já faremos a conexão com o banco de dados e
 * instanciaremos o objeto.
 */
  require_once "config/dbo-database.php";
  $conexaoComBancoDeDados = new DBODatabase('crud');
  // Como na tela de cadastro, encapsulamos o comando SQL dentro do objeto PHP.
  $pegaTodosDados = "SELECT id,nome,endereco FROM crudizinho";
  $getAll = $conexaoComBancoDeDados->execQuery($pegaTodosDados);
?>
<html>
<head>
    <title>Cadastro básico do CrUdS</title>
</head>
<body>

    <a href="index.php">Voltar para Index</a>
    <br/>

    <p><h1>Pesquisa dos campos cadastrados</h1>

    <table border="1" style="border: dashed blue 1px;">
        <tr>
            <th colspan="5">Listagem de dados cadastrados</th>
        </tr>
        <tr>
            <td>ID</td>
            <td>NOME</td>
            <td>ENDEREÇO</td>
            <td>ALTERAR</td>
            <td>EXCLUIR</td>
        </tr>
        <tr>
            <?php

                // Agora vem a mágica, o HTML misturado com o código dinâmico para fazer listar
                // os dados já cadastrados.
                foreach ($getAll as $g){
                    // Estamos dentro da tabela, colocamos a primeira linha fora do lado, porque é somente o índice.
                    // os dados serão mostrados pela variavel $g
                    // para mais informações, php.net
                    ?>
                        <td> <?php echo $g['id']; ?></td>
                        <td> <?php echo $g['nome']; ?></td>
                        <td> <?php echo $g['endereco']; ?></td>
                        <td><a href="alt.php?id=<?php echo $g['id']; ?>">alterar</a> </td>
                        <td><a href="exc.php?id=<?php echo $g['id']; ?>">excluir</a> </td>
                    </tr>
            <?php
                }
            ?>
    </table>

</body>
</html>