<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Nome da conexão padrão da base de dados
    |--------------------------------------------------------------------------
    |
    | Aqui você deve especificar o nome da conexão que será utilizada pelos scripts AGI
    | *** O nome da conexão aqui deve existir no array 'connections' abaixo
	|
    |
    */

    'default' => 'mysql',

    /*
    |--------------------------------------------------------------------------
    | Lista de conexões disponíveis
    |--------------------------------------------------------------------------
    |
    | Aqui deverá ser especificado a listagem de conexões disoníveis a ser utilizada pelos
	| scripts AGI
    |
    */

    'connections' => array(

        'crud' => array(
            'host'      => '127.0.0.1',
            'database'  => 'crud',
            'username'  => 'root',
            'password'  => '123123',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci'
        ),

    ),

);

